# KCIC Recruiting Challenge

## Tasks

Your mission, should you choose to accept it is to:

*   Protect the Approval Queue route from unauthenticated users, showing an error page when the route is accessed explicitly
*   Implement the ability to review claims in the queue (i.e. accept/reject with a reason provided)
*   Add a status column to the Queue to display the status of each claim.
*   Order the queue such that un-reviewed claims (those with "Pending" status) appear at the top of the queue; the rest of the queue should be sorted in ascending order.



## Business Logic Rules

*   Only authenticated users can view the queue
*   Only authenticated users can see the nav button for the queue
*   Unauthenticated users who manually navigate to the **queue** route should be shown an error page indicating the need to login.
*   Only authenticated users can add new warranty claims to the queue
*   Online ApproverUser can approve/reject warranty claims in the queue
*   Warranty claim approval and rejection each require a rationale (ReviewRationale) to be provided by the end user.
*   Submitter user cannot approve any claims.
*   ApproverUser cannot review (approve/reject) claims they have submitted themselves.
*   An approved claim should be set to status "Approved" and a rejected claim set to the status "Rejected".


## Project Notes, Submission Instructions, and Tips


This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

*   **[Create a private fork of this repository](https://confluence.atlassian.com/bitbucket/files/304578655/933101240/2/1509043649379/fork.gif), named ApprovalQueue_yourlastname** and provide read access to Bitbucket user **crookc**
* The application is built with .NET Core 2.2.
* Full instructions for the assignment are available on the home page (Home.js) of the running application.
* There are no database dependencies for this application.
* The application can be run through Visual Studio, or from the command line using ```dotnet run KCICApprovalQueue```
* Please **create a feature branch off of the ```develop``` branch entitled: ```feature/lastname``` (substituting in your last name)**
* Make as many or as few commits to that branch as necessary to that branch, and **submit a pull request into the ```develop``` when finished**.

If you have any questions please contact Chris Crook at [developers@kcic.com](mailto:developers@kcic.com)

*   Unit and integration tests are encouraged but not required.
*   Project is a **create-react-app** template utilzing Redux for global state.
*   **react-jsonschema-form** is utilized for form creation on the existing **AddClaimModal** and is pre-populating values to make manual testing less tedious.
*   Data persistence is provided by a static class, not a database, for simplicity.
*   "Authentication" is faked and the current user (as selected by the dropdown in the header) is available in the global state store at **state.auth.currentUser.**
*   **BaseService** injects a "FakeAuth" header that contains the current user's username
*   FakeAuthMiddleware is used to generate a ClaimsIdentity server-side, available from **HttpContext.User.Identity**
*   **submitter** and **approver** roles are also built into the ClaimsIdentity


## UI Examples

#### Queue

![Example queue](KCICApprovalQueue/ClientApp/public/docs/ExampleUI.png)

#### Review Modal

![Example review modal](KCICApprovalQueue/ClientApp/public/docs/ModalExample.png)

